import pymysql


class DataBase:
    def __init__(self):
        self.connection = pymysql.connect(
            host='localhost',
            user='root',
            password='12345',
            db='BikesCenter'
        )

        self.cursor = self.connection.cursor()
        print('Conexión exitosa')

# TODO: Aqui se piden todos los usuarios de la base de datos
    def get_clients(self):
        sql = 'select  documento_Cliente,  nombre, apellido, usuario, password, direccion, id_bike from cliente'
        try:
            self.cursor.execute(sql)
            return self.cursor.fetchall()

        except Exception as e:
            raise

# TODO: Aqui se muestra el usuario dependiendo el numero de documento
    def get_client(self, documento_Cliente):
        sql = 'select  documento_Cliente,  nombre, apellido, usuario, password, direccion, id_bike from cliente where documento_Cliente = {}'.format(
            documento_Cliente)

        try:
            self.cursor.execute(sql)
            return self.cursor.fetchone()

        except Exception as e:
            raise

# TODO: Aqui se insertan los datos de un nuevo usuario
    def insert_client(self, documento_Cliente,  nombre, apellido, usuario, password, direccion, id_Bike):
        sql = f'INSERT INTO cliente (documento_Cliente, nombre, apellido, usuario, password, direccion, id_Bike) VALUES(\'{documento_Cliente}\', \'{nombre}\', \'{apellido}\', \'{usuario}\', \'{password}\', \'{direccion}\', \'{id_Bike}\')'
        print(sql)
        try:
            self.cursor.execute(sql)
            self.connection.commit()

        except Exception as e:
            raise


    def insert_Bike(self, id_Bike,  marca, tipo):
        sql = f'INSERT INTO bike (id_Bike, marca, tipo) VALUES(\'{id_Bike}\', \'{marca}\', \'{tipo}\')'
        print(sql)
        try:
            self.cursor.execute(sql)
            self.connection.commit()

        except Exception as e:
            raise
# TODO: Aqui se borra un usuario dependiendo el numero de documento
    def delete_Client(self, numero_documeto):
        sql = 'DELETE FROM cliente WHERE numero_documeto = {}'.format(
            numero_documeto)
        print(sql)
        try:
            self.cursor.execute(sql)
        except Exception as e:
            raise

db = DataBase()
# print(db.get_clients())
print(db.get_client(7))
# db.insert_Bike(1,"GW", "Todo terreno")
# db.insert_client(7, 'Carlos', 'Nuñes', 'Carlitos',
#                  'akgdfbl', 'Crr 45 #45-46', 1)
# db.delete_Client(7)
